import React from 'react';
import {Form, Input, Select, Row, Col, Checkbox} from 'antd';


const formItemLayout = {
    labelCol: {span: 5},
    wrapperCol: {span: 19},
};


/**
 * 属性列表
 */
class AttributeWall extends React.Component {

    constructor(props) {
        super(props);
        this.getFieldDecorator = this.props.form.getFieldDecorator;
    }

    renderInput(attr, index) {
        return (
            <Col span={8} key={index}>
                <Form.Item {...formItemLayout} label={attr.name}>
                    {this.getFieldDecorator(`attributes[${attr.id}]` , {
                        rules: [{required: `attributes[${attr.id}]`.required == 1}]
                    })(
                        <Input placeholder={`填写${attr.name}`}/>
                    )}
                </Form.Item>
            </Col>
        )
    }

    renderBoolean(attr, index) {
        return (
            <Col span={8} key={index}>
                <Form.Item {...formItemLayout} label={attr.name}>
                    {this.getFieldDecorator(`attributes[${attr.id}]`, {
                        rules: [{required: `attributes[${attr.id}]`.required}]
                    })(
                        <Checkbox/>
                    )}
                </Form.Item>
            </Col>
        )
    }

    renderSelect(attr, index) {
        const options = attr.options.map((option, index) => {
            return (
                <Select.Option value={option.id} key={index}>{option.value}</Select.Option>
            )
        });
        return (
            <Col span={8} key={index}>
                <Form.Item {...formItemLayout} label={attr.name}>
                    {this.getFieldDecorator(`attributes[${attr.id}]`, {
                        rules: [{required: `attributes[${attr.id}]`.required}]
                    })(
                        <Select
                            showSearch
                            placeholder={`填写${attr.name}`}
                            optionFilterProp="children"
                        >
                            {options}
                        </Select>
                    )}
                </Form.Item>
            </Col>
        )
    }

    render() {
        let attributeFields = this.props.attributes.map((attr, index) => {
            if (attr.inputType === 'M') {
                return this.renderInput(attr, index);
            }
            if (attr.inputType === 'B') {
                return this.renderBoolean(attr, index);
            }
            if (attr.inputType === 'L') {
                return this.renderSelect(attr, index);
            }
        });

        return (
            <Row gutter={40}>
                {attributeFields}
            </Row>
        )
    }

}

export default AttributeWall;