import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

class SiderCustom extends Component {
    state = {
        collapsed: false,
        mode: 'inline',
        openKey: '',
        selectedKey: ''
    };
    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
        this.onCollapse(nextProps.collapsed);
    }
    onCollapse = (collapsed) => {
        this.setState({
            collapsed,
            mode: collapsed ? 'vertical' : 'inline',
        });
    };
    menuClick = e => {
        this.setState({
            selectedKey: e.key
        });
    };
    openMenu = v => {
        this.setState({
            openKey: v[v.length - 1]
        })
    };
    render() {
        return (
            <Sider
                trigger={null}
                breakpoint="lg"
                collapsed={this.props.collapsed}
                style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}
            >
                <div className="App-logo" />
                <Menu
                    onClick={this.menuClick}
                    theme="dark"
                    mode={this.state.mode}
                    selectedKeys={[this.state.selectedKey]}
                    openKeys={[this.state.openKey]}
                    onOpenChange={this.openMenu}
                >
                    <Menu.Item key="/dashboard">
                        <Link to={'/dashboard'}><Icon type="mobile" /><span className="nav-text">首页</span></Link>
                    </Menu.Item>
                    <SubMenu key="/product" title={<span><Icon type="scan" /><span className="nav-text">商品管理</span></span>}>
                        <Menu.Item key="/product/create"><Link to={'/product/create'}>创建商品</Link> </Menu.Item>
                        <Menu.Item key="/products"><Link to={'/products'}>商品列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/order" title={<span><Icon type="scan" /><span className="nav-text">订单管理</span></span>}>
                        <Menu.Item key="/orders"><Link to={'/orders'}>订单列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/category" title={<span><Icon type="scan" /><span className="nav-text">分类管理</span></span>}>
                        <Menu.Item key="/category/create"><Link to={'/category/create'}>创建分类</Link> </Menu.Item>
                        <Menu.Item key="/categories"><Link to={'/categories'}>分类列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/attribute" title={<span><Icon type="scan" /><span className="nav-text">属性管理</span></span>}>
                        <Menu.Item key="/attribute/create"><Link to={'/attribute/create'}>创建属性</Link> </Menu.Item>
                        <Menu.Item key="/attributes"><Link to={'/attributes'}>属性列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/spec" title={<span><Icon type="scan" /><span className="nav-text">规格管理</span></span>}>
                        <Menu.Item key="/spec/create"><Link to={'/spec/create'}>创建规格</Link> </Menu.Item>
                        <Menu.Item key="/specs"><Link to={'/specs'}>规格列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/brand" title={<span><Icon type="scan" /><span className="nav-text">品牌管理</span></span>}>
                        <Menu.Item key="/brand/create"><Link to={'/brand/create'}>创建品牌</Link> </Menu.Item>
                        <Menu.Item key="/brands"><Link to={'/brands'}>品牌列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/promotion" title={<span><Icon type="scan" /><span className="nav-text">促销管理</span></span>}>
                        <Menu.Item key="/promotion/product"><Link to={'/promotion/product'}>商品促销</Link> </Menu.Item>
                        <Menu.Item key="/promotion/order"><Link to={'/promotion/order'}>订单促销</Link> </Menu.Item>
                        <Menu.Item key="/promotion/group"><Link to={'/promotion/group'}>团购管理</Link> </Menu.Item>
                        <Menu.Item key="/seckill"><Link to={'/promotion/seckill'}>秒杀管理</Link> </Menu.Item>
                        <Menu.Item key="/coupon"><Link to={'/coupons'}>优惠券管理</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/user" title={<span><Icon type="scan" /><span className="nav-text">用户管理</span></span>}>
                        <Menu.Item key="/users"><Link to={'/users'}>用户列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="system"
                        title={<span><Icon type="switcher" /><span className="nav-text">系统</span></span>}
                    >
                        <Menu.Item key="/system/settings"><Link to={'/system/settings'}>系统配置</Link></Menu.Item>
                        <Menu.Item key="/system/home"><Link to={'/system/home'}>首页配置</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="statistics"
                        title={<span><Icon type="switcher" /><span className="nav-text">统计</span></span>}
                    >
                        <Menu.Item key="/statistics"><Link to={'/statistics'}>统计</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="pages"
                        title={<span><Icon type="switcher" /><span className="nav-text">页面</span></span>}
                    >
                        <Menu.Item key="/login"><Link to={'/login'}>登录</Link></Menu.Item>
                        <Menu.Item key="/404"><Link to={'/404'}>404</Link></Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
        )
    }
}

export default SiderCustom;