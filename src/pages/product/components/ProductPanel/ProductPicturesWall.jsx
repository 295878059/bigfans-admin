import React from 'react';
import {Upload, Icon, Modal, Form, Input} from 'antd';
import PicturesWall from 'components/PicturesWall/PicturesWall'
import HttpUtils from 'utils/HttpUtils'

class ProductPicturesWall extends PicturesWall {

    constructor(props) {
        super(props);
        this.maxsize = 8;
        this.fieldName = `products[${this.props.prodIndex}][imgs]`;
        this.state.imgs = [];
    }

    getUploadUrl(){
        return HttpUtils.productServiceUrl + '/uploadProductImg';
    }

    getRemoveUrl() {
        return HttpUtils.productServiceUrl + '/removeProductImg';
    }

    onUploaded(file){
        let productImgs = this.props.form.getFieldValue(this.fieldName) || [];
        let imgs = this.state.imgs;
        imgs.push(file.response.data.fileKey);
        this.setState({imgs});
    }

    handleRemove = (file) => {
        if(!file.response){
            return;
        }
        var data = {
            file : file.response.data.fileKey
        }
        fetch(this.getRemoveUrl(),{
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(data)
        })
        .then(res => res.json());
    }

    renderFormField(fileList) {
        return (
            <Form.Item>
                {this.props.form.getFieldDecorator(this.fieldName , {
                    initialValue:this.state.imgs
                })(
                    <Input type="hidden"/>
                )}
            </Form.Item>
        )
    }
}

export default ProductPicturesWall;