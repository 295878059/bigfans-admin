import React from 'react';
import LzEditor from 'react-lz-editor'

class ProductIntroductionEditor extends React.Component {

	constructor(props) {
	    super(props);
	    this.state = {
	      htmlContent: ``,
	      responseList: []
	    }
	    this.receiveHtml=this.receiveHtml.bind(this);
    }

  	receiveHtml(content) {
    	this.props.form.setFieldsValue({'productGroup.description' : content});
    	this.setState({responseList:[]});
  	}

  	render() {
	    let policy = "";
	    const uploadProps = {
	      action: "http://v0.api.upyun.com/devopee",
	      onChange: this.onChange,
	      listType: 'picture',
	      fileList: this.state.responseList,
	      data: (file) => {

	      },
	      multiple: true,
	      beforeUpload: this.beforeUpload,
	      showUploadList: true
	    }
	    return (
	      	<div>
		        <LzEditor 
		        active={true} 
		        importContent={this.state.htmlContent} 
		        cbReceiver={this.receiveHtml} 
		        uploadProps={uploadProps}
		        lang="en"/>
	        </div>
	    );
  	}
}

export default ProductIntroductionEditor;