import React from 'react';
import {Breadcrumb} from 'antd';
import {Card} from 'antd';
import AttrCreateFrom from './form'

class AttrCreatePage extends React.Component {

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>属性管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建属性</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <AttrCreateFrom history={this.props.history}/>
                </Card>
            </div>
        );
    }
}

export default AttrCreatePage;