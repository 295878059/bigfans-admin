import React from 'react';
import {Button ,Form , Row, Col , Input, message} from 'antd'
import AppHelper from 'utils/AppHelper'
import 'whatwg-fetch'

const data=[{
		  key: '1',
		  name: 'John Brown',
		  age: 32,
		  address: 'New York No. 1 Lake Park',
		}, {
		  key: '2',
		  name: 'Jim Green',
		  age: 42,
		  address: 'London No. 1 Lake Park',
		}, {
		  key: '3',
		  name: 'Joe Black',
		  age: 32,
		  address: 'Sidney No. 1 Lake Park',
		}, {
		  key: '4',
		  name: 'Disabled User',
		  age: 99,
		  address: 'Sidney No. 1 Lake Park',
		}]

class SearchForm extends React.Component {

	state = {
		parentCats : []
	}

	constructor(props) {
		super(props);
		this.searchUrl = AppHelper.config.serviceUrl + '/products';
	}

	handleBrandChange = () => {

	}

	handleReset = () => {
		this.props.form.resetFields();
	}

	handleSearch = (e) => {
		let values = this.props.form.getFieldsValue();

		this.props.changeLoading(true);
		fetch(AppHelper.api.buildGetUrl(this.searchUrl , values) , {
            method : 'GET',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
        }).then(res => res.json())
        .then((resp) => {
        	this.props.receiveData(data);
        	this.props.changeLoading(false);
            message.destroy();
        })
        .catch((error) => {
            message.destroy();
            this.props.receiveData(data);
            this.props.changeLoading(false);
        })
	}

	render () {
		const {getFieldDecorator} = this.props.form;
		const formItemLayout = {
	      labelCol: { span: 5 },
	      wrapperCol: { span: 18 },
	    };
		return (
			<Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
				<Row gutter={24}>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="手机或者邮箱" >
		                    {getFieldDecorator('name4')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
                </Row>
                <Row>
                	<Col span={24} style={{ textAlign: 'right' }}>
			            <Button type="primary" htmlType="submit">Search</Button>
			            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
			              Clear
			            </Button>
			        </Col>
                </Row>
			</Form>

			)
	}
}

const ProductSearchForm = Form.create()(SearchForm);
export default ProductSearchForm;
