import React from 'react';
import {Form, Input, Breadcrumb, Card, InputNumber, Select, DatePicker , Button, Col , Table , message} from 'antd';
import BaseComponent from 'components/BaseComponent'
import ProductSearchModal from 'pages/product/components/ProductSearchModal'
import AppHelper from 'utils/AppHelper';

const Option = Select.Option;
const {TextArea} = Input

const productColumns = [{
  title: '商品名',
  dataIndex: 'name',
  key: 'name',
}, {
  title: '规格',
  dataIndex: 'spec',
  key: 'spec',
}, {
  title: '价格',
  dataIndex: 'age',
  key: 'age',
}, {
  title: '库存',
  dataIndex: 'address',
  key: 'address',
}, {
  title: '操作',
  dataIndex: 'operation',
  key: 'operation',
  render : function(){
    return (<div>删除</div>)
  }
}];

class SeckillCreate extends BaseComponent{

    state = {
        searchProdVisible : false,
        selectedProducts : [],
        productSearchModalKey : 1
    }

    constructor(props){
        super(props)
        this.createUrl = AppHelper.config.serviceUrl + '/seckill'
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        //let name = this.props.form.getFieldValue('name');
        //let origin = this.props.form.getFieldValue('origin');
        // 获取表单中数据
        let formVals = this.props.form.getFieldsValue();
        formVals.productIdList = AppHelper.Arrays.pluck(this.state.selectedProducts , 'key');
        console.info(formVals);
        message.loading('保存中',0);
        fetch(this.createUrl , {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(formVals)
        }).then(res => res.json())
        .then((resp) => {
            message.destroy();
            console.info('success')
        })
        .catch((error) => {
            message.destroy();
        })
    }

    handleTypeChange = (type) => {
        this.setState({type});
    }

    showSearchProdModal = () =>{
        let modalKey = this.state.productSearchModalKey;
        this.setState({searchProdVisible : true , productSearchModalKey : modalKey +1})
    }

    handleSearchProdOk = (data) =>{
        let currentData = this.state.selectedProducts;
        let targetData = data.concat(currentData);
        this.setState({searchProdVisible : false , selectedProducts:targetData});
    }

    handleSearchProdCancel = () => {
        this.setState({searchProdVisible : false})
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>促销管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建秒杀</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <Form.Item {...formItemLayout} label="秒杀场次" hasFeedback >
                            {getFieldDecorator('timezone', {
                                rules: [{required: true, message: '请填写时间!'}],
                            })(
                            <Select defaultValue="1012">
                              <Option value="0006">00:00 - 06:00</Option>
                              <Option value="0610">06:00 - 10:00</Option>
                              <Option value="1012">10:00 - 12:00</Option>
                              <Option value="1217">12:00 - 17:00</Option>
                              <Option value="1722">17:00 - 22:00</Option>
                            </Select>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="抢购价" hasFeedback >
                            {getFieldDecorator('seckillPrice', {
                                rules: [{required: true, message: '请填写抢购价!'}],
                            })(
                                <InputNumber style={{width:'auto'}}/>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="抢购数量" hasFeedback>
                            {getFieldDecorator('amount', {
                                rules: [{required: true, message: '请填写抢购数量!'}],
                            })(
                                <InputNumber style={{width:'auto'}} min={1}/>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="每人限购数量" hasFeedback>
                            {getFieldDecorator('limitBuycount', {
                                rules: [{required: true, message: '请填写每人限购!'}],
                            })(
                                <InputNumber style={{width:'auto'}} min={1}/>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="活动期限" hasFeedback>
                            <Col span={5}>
                            <Form.Item>
                                {getFieldDecorator('startTime',{
                                    rules: [{required: true}],
                                })(
                                    <DatePicker
                                      dateFormat = 'YYYY/MM/DD'
                                      placeholder="开始日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                          <Col span={2}>
                            <p className="ant-form-split">-</p>
                          </Col>
                          <Col span={4}>
                            <Form.Item>
                                {getFieldDecorator('endTime',{
                                    rules: [{required: true, message: ''}],
                                })(
                                    <DatePicker
                                      dateFormat = 'YYYY/MM/DD'
                                      placeholder="结束日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="选择活动商品" hasFeedback >
                            <Button type="primary" onClick={this.showSearchProdModal}>选择商品</Button>
                            <ProductSearchModal key={this.state.productSearchModalKey} 
                                                visible={this.state.searchProdVisible} 
                                                onOk={this.handleSearchProdOk} 
                                                onCancel={this.handleSearchProdCancel}/>
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="已选择的商品" hasFeedback >
                            <Table pagination={false} size='small' dataSource={this.state.selectedProducts} columns={productColumns} />
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="活动描述" hasFeedback>
                            {getFieldDecorator('description')(
                                <TextArea rows={4} placeholder="活动描述信息"/>
                            )}
                        </Form.Item>                    
                        <Form.Item {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" size="large">确认</Button>
                        </Form.Item>
                    </Form>
                </Card>
            </div>
        );
    }
}

export default Form.create()(SeckillCreate);
